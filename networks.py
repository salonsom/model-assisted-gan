'''
Networks implementation.

Enhanced version of (reference):
    - Image-based model parameter optimization using Model-Assisted Generative Adversarial Networks
        *[DOI] https://doi.org/10.1016/j.eswa.2020.113250
        *[arXiv] https://arxiv.org/abs/1812.00879
'''
__version__ = '1.1'
__author__ = 'Saul Alonso-Monsalve, Leigh Howard Whitehead'
__email__ = 'saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'

import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Activation, Flatten, Reshape
from tensorflow.keras.layers import Conv1D, Conv2D, Conv2DTranspose, UpSampling2D
from tensorflow.keras.layers import LeakyReLU, Dropout, Lambda
from tensorflow.keras.optimizers import Adam, RMSprop
from tensorflow.keras import initializers
from tensorflow_addons.layers import InstanceNormalization

class Networks(object):
    def __init__(self, noise_size=100, params=4, img_rows=32, img_cols=32, channels=1,
                 WGAN=False, print_summary=False, instance_normalization=False):
        ''' Constructor.

        Args:
            noise_size: size of input noise to the generator.
            params: number of model parameters.
            img_rows: image rows.
            img_cols: image columns.
            channels: image channels.
            WGAN: Wasserstein GAN.
            instance_normalization: instance normalization.  
            print_summary: print summary of the models.
        '''
        self.noise_size = noise_size
        self.params = params
        self.img_rows = img_rows
        self.img_cols = img_cols
        self.channels = channels
        self.WGAN = WGAN
        self.instance_normalization = instance_normalization
        self.print_summary = print_summary

        self.G = None # generator
        self.E = None # emulator
        self.D = None # discriminator
        self.S = None # siamese
        self.SM = None # siamese model
        self.AM1 = None # adversarial model 1 (emulator + siamese)
        self.DM = None # discriminator model
        self.AM2 = None # adversarial model 2 (generator + emulator + discriminator)

    def wasserstein_loss(self, y_true, y_pred):
        ''' Wasserstein loss for WGAN.

        Args:
            y_true: actual values.
            y_pred: predicted scores.

        Returns: multiplying the actual labels by the predicted scores,
                 then calculating the mean.
        '''
        return K.mean(y_true * y_pred)
 
    def emulator(self, dropout=0.0, upsampling='conv2dtranspose'):
        ''' Emulator: generate identical images to those of the simulator S when both E and S 
        are fed with the same input parameters.

        Args:
            dropout: dropout value.
            upsampling: type of upsampling ('conv2dtranspose':Conv2DTranspose or else:UpSampling2D+Conv2D)

        Returns: emulator model.
        ''' 
        if self.E:
            return self.E

        # input params
        input_params_shape = (self.params,)
        input_params_layer = Input(shape=input_params_shape, name='input_params')

        # architecture
        self.E = Reshape((self.params//2, self.params//2, 1))(input_params_layer)
        if upsampling == 'conv2dtranspose':
            self.E = Conv2DTranspose(128, kernel_size=(5,5), strides=2, padding='same',
                                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        else:
            self.E = UpSampling2D(size=(2, 2))(self.E)
            self.E = Conv2D(128, kernel_size=(5, 5), padding='same',
                            kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        if self.instance_normalization:
            self.E = InstanceNormalization(axis=-1, center=True, scale=True)(self.E)
        self.E = Activation('relu')(self.E)
        self.E = Dropout(dropout)(self.E)
        if upsampling == 'conv2dtranspose':
            self.E = Conv2DTranspose(64, kernel_size=(5,5), strides=2, padding='same',
                                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        else:
            self.E = UpSampling2D(size=(2, 2))(self.E)
            self.E = Conv2D(64, kernel_size=(5, 5), padding='same',
                            kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        if self.instance_normalization:
            self.E = InstanceNormalization(axis=-1, center=True, scale=True)(self.E)
        self.E = Activation('relu')(self.E)
        self.E = Dropout(dropout)(self.E)
        if upsampling == 'conv2dtranspose':
            self.E = Conv2DTranspose(32, kernel_size=(5,5), strides=2, padding='same',
                                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        else:
            self.E = UpSampling2D(size=(2, 2))(self.E)
            self.E = Conv2D(32, kernel_size=(5, 5), padding='same',
                            kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        if self.instance_normalization:
            self.E = InstanceNormalization(axis=-1, center=True, scale=True)(self.E)
        self.E = Activation('relu')(self.E)
        self.E = Dropout(dropout)(self.E)
        if upsampling == 'conv2dtranspose':
            self.E = Conv2DTranspose(1, kernel_size=(5,5), strides=2, padding='same',
                                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        else:
            self.E = UpSampling2D(size=(2, 2))(self.E)
            self.E = Conv2D(1, kernel_size=(5, 5), padding='same',
                            kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
        self.E = Activation('tanh')(self.E)

        # model
        self.E = Model(inputs=input_params_layer, outputs=self.E, name='emulator')

        # print
        if self.print_summary:
            print('Emulator')
            self.E.summary()

        return self.E

    def siamese(self, dropout=0.0):
        ''' Siamese: determine the similarity between images produced by the simulator and the emulator.

        Args:
            dropout: dropout value.

        Returns: siamese model.
        '''
        if self.S:
            return self.S

        # input images
        input_shape_image = (self.img_rows, self.img_cols, self.channels)
        input_image_anchor = Input(shape=input_shape_image, name='input_image_anchor')
        input_image_candid = Input(shape=input_shape_image, name='input_image_candidate')
        input_image = Input(shape=input_shape_image, name='input_image')

        # siamese
        cnn = Conv2D(16, kernel_size=(8, 8), strides=(2, 2), padding='same', 
                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(input_image)
        cnn = LeakyReLU(0.2)(cnn)
        cnn = Dropout(dropout)(cnn)
        cnn = Conv2D(32, kernel_size=(8, 8), strides=(2, 2), padding='same',
                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
        if self.instance_normalization:
            cnn = InstanceNormalization(axis=-1, center=True, scale=True)(cnn)
        cnn = LeakyReLU(0.2)(cnn)
        cnn = Dropout(dropout)(cnn)
        cnn = Conv2D(64, kernel_size=(5, 5), strides=(2, 2), padding='same',
                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
        if self.instance_normalization:
            cnn = InstanceNormalization(axis=-1, center=True, scale=True)(cnn)
        cnn = LeakyReLU(0.2)(cnn)
        cnn = Dropout(dropout)(cnn)
        cnn = Conv2D(128, kernel_size=(5, 5), strides=(2, 2), padding='same',
                     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
        if self.instance_normalization:
            cnn = InstanceNormalization(axis=-1, center=True, scale=True)(cnn)
        cnn = LeakyReLU(0.2)(cnn)
        cnn = Dropout(dropout)(cnn)
        cnn = Flatten()(cnn)
        cnn = Activation('sigmoid')(cnn)
        cnn = Model(inputs=input_image, outputs=cnn, name='cnn')

        if self.print_summary:
            print('CNN:')
            cnn.summary()

        # left and right encodings         
        encoded_l = cnn(input_image_anchor)
        encoded_r = cnn(input_image_candid)

        # merge two encoded inputs with the L1 or L2 distance between them
        L1_distance = lambda x: K.abs(x[0]-x[1])
        L2_distance = lambda x: (x[0]-x[1]+K.epsilon())**2/(x[0]+x[1]+K.epsilon())
        both = Lambda(L2_distance)([encoded_l, encoded_r])
        prediction = Dense(1)(both)

        if not self.WGAN:
            prediction = Activation('sigmoid')(prediction)

        # model
        self.S = Model(inputs=[input_image_anchor, input_image_candid], outputs=prediction, name='siamese')

        # print
        if self.print_summary:
            print('Siamese:')
            self.S.summary()

        return self.S

    def generator(self, dropout=0.0):
        ''' Generator: produce parameters such that the simulator can use them to create images that 
        cannot be distinguished from true data images.

        Args:
            dropout: dropout value.

        Returns: generator model.        
        '''
        if self.G:
            return self.G

        # input noise
        input_noise_shape = (self.noise_size,)
        input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

        # architecture
        self.G = Reshape((self.noise_size, 1))(input_noise_layer)
        self.G = Conv1D(64, kernel_size=5, strides=2,
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.G)
        if self.instance_normalization:
            self.G = InstanceNormalization(axis=-1, center=True, scale=True)(self.G)
        self.G = Activation('relu')(self.G)
        self.G = Dropout(dropout)(self.G)
        self.G = Conv1D(32, kernel_size=5, strides=2,
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.G)
        if self.instance_normalization:
            self.G = InstanceNormalization(axis=-1, center=True, scale=True)(self.G)
        self.G = Activation('relu')(self.G)
        self.G = Dropout(dropout)(self.G)
        self.G = Conv1D(16, kernel_size=5, strides=2,
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.G)
        if self.instance_normalization:
            self.G = InstanceNormalization(axis=-1, center=True, scale=True)(self.G)
        self.G = Activation('relu')(self.G)
        self.G = Dropout(dropout)(self.G)
        self.G = Conv1D(1, kernel_size=6, strides=1,
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.G)
        self.G = Activation('tanh')(self.G)

        # model
        self.G = Model(inputs=input_noise_layer, outputs=self.G, name='generator')

        # print
        if self.print_summary:
            print('Generator:')
            self.G.summary()

        return self.G

    def discriminator(self, dropout=0.0):
        ''' Discriminator: distinguish between true data images and images produced by the simulator (or 
        images produced by the emulator to speed up the training process).

        Args:
            dropout: dropout value.

        Returns: discriminator model.
        '''
        if self.D:
            return self.D

        # input image
        input_shape_image = (self.img_rows, self.img_cols, self.channels)
        input_layer_image = Input(shape=input_shape_image, name='input_image')

        # architecture
        self.D = Conv2D(32, kernel_size=(8, 8), strides=(2, 2), padding='same', 
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(input_layer_image)
        if self.instance_normalization:
            self.D = InstanceNormalization(axis=-1, center=True, scale=True)(self.D)
        self.D = LeakyReLU(0.2)(self.D)
        self.D = Dropout(dropout)(self.D)
        self.D = Conv2D(64, kernel_size=(5, 5), strides=(2, 2), padding='same',
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.D)
        if self.instance_normalization:
            self.D = InstanceNormalization(axis=-1, center=True, scale=True)(self.D)
        self.D = LeakyReLU(0.2)(self.D) 
        self.D = Dropout(dropout)(self.D)
        self.D = Conv2D(128, kernel_size=(5, 5), strides=(2, 2), padding='same',
                        kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.D)
        if self.instance_normalization:
            self.D = InstanceNormalization(axis=-1, center=True, scale=True)(self.D)
        self.D = LeakyReLU(0.2)(self.D)
        self.D = Dropout(dropout)(self.D)
        self.D = Flatten()(self.D)
        self.D = Dense(1)(self.D)
        if not self.WGAN:
            self.D = Activation('sigmoid')(self.D)

        # model
        self.D = Model(inputs=input_layer_image, outputs=self.D, name='discriminator')

        # print
        if self.print_summary:
            print('Discriminator:')
            self.D.summary()

        return self.D

    def siamese_model(self, lr=0.0002):
        ''' Siamese model.

        Args:
            lr: learning rate.

        Returns: siamese model (compiled).
        '''
        if self.SM:
            return self.SM

        # optimizer
        if self.WGAN:
            optimizer = RMSprop(lr=lr)
        else:
            optimizer = Adam(lr=lr, beta_1=0.5)

        # input images
        input_shape_image = (self.img_rows, self.img_cols, self.channels)
        input_image_anchor = Input(shape=input_shape_image, name='input_image_anchor')
        input_image_candid = Input(shape=input_shape_image, name='input_image_candidate')
        input_layer = [input_image_anchor, input_image_candid]

        # discriminator
        siamese_ref = self.siamese()
        siamese_ref.trainable = True
        self.SM = siamese_ref(input_layer)

        # model
        self.SM = Model(inputs=input_layer, outputs=self.SM, name='siamese_model')
        if self.WGAN:
            self.SM.compile(loss=self.wasserstein_loss, optimizer=optimizer,\
                metrics=['accuracy'])
        else:
            self.SM.compile(loss='binary_crossentropy', optimizer=optimizer,\
                metrics=['accuracy'])
 
        if self.print_summary:
            print('Siamese model') 
            self.SM.summary()

        return self.SM


    def discriminator_model(self, lr=0.0002):
        ''' Discriminator model.

        Args:
            lr: learning rate.

        Returns: discriminator model (compiled).
        '''
        if self.DM:
            return self.DM

        # optimizer
        if self.WGAN:
            optimizer = RMSprop(lr=lr)
        else:
            optimizer = Adam(lr=lr, beta_1=0.5)

        # input image
        input_image_shape = (self.img_rows, self.img_cols, self.channels)
        input_image_layer = Input(shape=input_image_shape, name='input_image')

        # discriminator
        discriminator_ref = self.discriminator()
        discriminator_ref.trainable = True
        self.DM = discriminator_ref(input_image_layer)

        # model
        self.DM = Model(inputs=input_image_layer, outputs=self.DM, name='discriminator_model')
        self.DM.compile(loss='binary_crossentropy', optimizer=optimizer,\
            metrics=['accuracy'])
        
        if self.print_summary:
            print('Discriminator model') 
            self.DM.summary()

        return self.DM

    def adversarial1_model(self, lr=0.0002):
        ''' Adversarial 1 model.

        Args:
            lr: learning rate.

        Returns: adversarial 1 model (compiled).
        '''
        if self.AM1:
            return self.AM1

        # optimizer
        if self.WGAN:
            optimizer = RMSprop(lr=lr)
        else:
            optimizer = Adam(lr=lr, beta_1=0.5)

        # input 1: simulated image
        input_image_shape = (self.img_rows, self.img_cols, self.channels)
        input_image_layer = Input(shape=input_image_shape, name='input_image')

        # input 2: params
        input_params_shape = (self.params,)
        input_params_layer = Input(shape=input_params_shape, name='input_params')

        # emulator
        emulator_ref = self.emulator()
        emulator_ref.trainable = True
        self.AM1 = emulator_ref(input_params_layer)

        # siamese
        siamese_ref = self.siamese()
        siamese_ref.trainable = False
        self.AM1 = siamese_ref([input_image_layer, self.AM1])

        # model
        input_layer = [input_image_layer, input_params_layer] 
        self.AM1 = Model(inputs=input_layer, outputs=self.AM1, name='adversarial_1_model')
        if self.WGAN:
            self.AM1.compile(loss=self.wasserstein_loss, optimizer=optimizer,\
                    metrics=['accuracy'])
        else:
            self.AM1.compile(loss='binary_crossentropy', optimizer=optimizer,\
                    metrics=['accuracy'])

        # print
        if self.print_summary:
            print('Adversarial 1 model:')
            self.AM1.summary()
 
        return self.AM1

    def adversarial2_model(self, lr=0.0002):
        ''' Adversarial 2 model.

        Args:
            lr: learning rate.

        Returns: adversarial 2 model (compiled).
        '''
        if self.AM2:
            return self.AM2

        # optimizer
        if self.WGAN:
            optimizer = RMSprop(lr=lr)
        else:
            optimizer = Adam(lr=lr, beta_1=0.5)

        # input noise
        input_noise_shape = (self.noise_size,)
        input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

        # generator
        generator_ref = self.generator()
        generator_ref.trainable = True
        self.AM2 = generator_ref(input_noise_layer)

        # emulator
        emulator_ref = self.emulator()
        emulator_ref.trainable = False
        self.AM2 = emulator_ref(self.AM2)

        # discriminator
        discriminator_ref = self.discriminator()
        discriminator_ref.trainable = False
        self.AM2 = discriminator_ref(self.AM2)

        # model
        self.AM2 = Model(inputs=input_noise_layer, outputs=self.AM2, name='adversarial_2_model')
        self.AM2.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])

        # print
        if self.print_summary:
            print('Adversarial 2 model')
            self.AM2.summary()
 
        return self.AM2
