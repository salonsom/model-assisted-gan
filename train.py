'''
Case study 1 implementation.

Enhanced version of (reference):
    - Image-based model parameter optimization using Model-Assisted Generative Adversarial Networks
        *[DOI] https://doi.org/10.1016/j.eswa.2020.113250
        *[arXiv] https://arxiv.org/abs/1812.00879
'''
__version__ = '1.1'
__author__ = 'Saul Alonso-Monsalve, Leigh Howard Whitehead'
__email__ = 'saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'

import numpy as np
import os
import matplotlib.pyplot as plt
import shutil
from pathlib import Path
from networks import Networks
from scipy.interpolate import interp1d
from argparse import ArgumentParser

# manually specify the GPUs to use
os.environ['CUDA_DEVICE_ORDER']='PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES']='0'

# remove for randomness
np.random.seed(7)

# Case study 1 ranges and mapping
net_range  = [-1,1]
m_range = [-2,2]
c_range = [-2,2]
x_range = [0,12]
s_range = [5,15]
m_m = interp1d(net_range,m_range)
m_c = interp1d(net_range,c_range)
m_x = interp1d(net_range,x_range)
m_s = interp1d(net_range,s_range)

def get_args():
    ''' Retrieve arguments.

    Returns: arguments.
    '''
    parser = ArgumentParser(description='Model-Assisted GAN')
    arg = parser.add_argument
    arg('--img_rows', type=int, default=32, help='Image rows')
    arg('--img_cols', type=int, default=32, help='Image columns')
    arg('--channels', type=int, default=1, help='Image channels')
    arg('--params', type=int, default=4, help='Model parameters')
    arg('--noise_size', type=int, default=100, help='Noise size (generator input)')
    arg('--WGAN', type=lambda x: (str(x).lower() == 'true'), default=False, help='Use Wasserstein GAN')
    arg('--pretrain', type=lambda x: (str(x).lower() == 'true'), default=True, help='Run pre-training')
    arg('--train', type=lambda x: (str(x).lower() == 'true'), default=False, help='Run training')
    arg('--pretrain_steps', type=int, default=1000000, help='Pre-training steps')
    arg('--train_steps', type=int, default=1000000, help='Training steps')
    arg('--batch_size', type=int, default=128, help='Batch size')
    arg('--print_summary', type=lambda x: (str(x).lower() == 'true'), default=False, help='Print summary of the models')
    arg('--save_figs', type=lambda x: (str(x).lower() == 'true'), default=False, help='Save figures')
    arg('--save_freq', type=int, default=10000, help='Figure saving frequency')
    arg('--figs_path', type=str, default=None, help='Figures path')
    arg('--n_figs', type=int, default=20, help='Number of images to save')
    arg('--load_weights', type=lambda x: (str(x).lower() == 'true'), default=False, help='Load pre-trained weights')
    arg('--save_weights', type=lambda x: (str(x).lower() == 'true'), default=False, help='Save weights')
    arg('--weights_path', type=str, default=None, help='Weights path')
 
    args = parser.parse_args()
    return args

args = get_args()

class ModelAssistedGAN(object):

    def __init__(self, noise_size=100, params=4, img_rows=32, img_cols=32, channels=1, WGAN=False, print_summary=False):
        ''' Constructor.
        
        Args:
            noise_size: size of input noise to the generator.
            params: number of model parameters.
            img_rows: image rows.
            img_cols: image columns.
            channels: image channels.
            WGAN: Wasserstein GAN.
            print_summary: print summary of the models.
        '''
        self.noise_size = noise_size
        self.params = params
        self.img_rows = img_rows
        self.img_cols = img_cols
        self.channels = channels
        self.WGAN = WGAN
        self.Networks = Networks(noise_size=noise_size, params=params, img_rows=img_rows, img_cols=img_cols,\
                                 channels=channels, WGAN=self.WGAN, print_summary=print_summary,\
                                 instance_normalization=True)
        self.discriminator = self.Networks.discriminator_model()
        self.siamese = self.Networks.siamese_model()
        self.adversarial1 = self.Networks.adversarial1_model()
        self.adversarial2 = self.Networks.adversarial2_model()
        self.generator = self.Networks.generator()
        self.emulator = self.Networks.emulator()

    def data_params(self):
        ''' Generate parameters for true data.

        Returns: true data parameters.
        '''
        while(True):
            m, c, x0, x_steps = [np.clip(np.random.normal(1.5, 0.3, 1), m_range[0], m_range[1])[0],
                                 np.clip(np.random.normal(0.5, 0.1, 1), c_range[0], c_range[1])[0], 
                                 np.clip(np.random.normal(5, 0.5, 1), x_range[0], x_range[1])[0], 
                                 np.clip(np.random.normal(9, 0.5, 1), s_range[0], s_range[1])[0]]
            y = int(round(m*x0+c))
            if y >= -(self.img_rows//2) and y<(self.img_rows//2):
                break
        return m, c, x0, x_steps
           
    # Fill pixel map for case study 1 
    def fillPixelMap(self, m, c, x0, x_steps, pixelMap):
        ''' Fill pixel map.
        
        Args:
            m: slope.
            c: constant.
            x0: initial x.
            x_steps: extent in the x-direction.
            pixelMap: array to fill.
        '''
        max_len = pixelMap.shape[0]
        value = 1.0
        x0 = int(np.around(x0))
        x_steps = int(np.around(x_steps)) 
        for x in range(x0, x0+x_steps):
            if x >= max_len or x < 0:
                continue
            y= int(np.around(m*x+c)) + max_len//2
            if y >= max_len or y < 0:
                continue
            pixelMap[x,y,0] = value


    def save_weights(self, path='weights'):
        ''' Save model weights.

        Args:
            path: weights path.
        '''
        print('Saving weights...')
        Path(path).mkdir(parents=True, exist_ok=True)
        self.siamese.save_weights(path + '/siamese_weights')
        self.discriminator.save_weights(path + '/discriminator_weights')
        self.emulator.save_weights(path + '/emulator_weights')
        self.adversarial1.save_weights(path + '/adversarial1_weights')
        self.adversarial2.save_weights(path + '/adversarial2_weights')
        self.generator.save_weights(path + '/generator_weights')

    def load_weights(self, path='weights'):
        ''' Load pre-trained model weights.

        Args:
            path: pre-trained weights path.
        '''
        print('Loading weights...')
        self.siamese.load_weights(path + '/siamese_weights')
        self.discriminator.load_weights(path + '/discriminator_weights')
        self.emulator.load_weights(path + '/emulator_weights')
        self.adversarial1.load_weights(path + '/adversarial1_weights')
        self.adversarial2.load_weights(path + '/adversarial2_weights')
        self.generator.load_weights(path + '/generator_weights')

    def pretrain(self, train_steps=1000000, batch_size=128, save_figs=False, save_freq=10000,\
                 figs_path=None, n_figs=20, load_weights=False, save_weights=False, weights_path=None):
        ''' Pre-training stage.

        Args:
            train_steps: number of pre-training steps (iterations).
            batch_size: batch size.
            save_figs: save figures.
            save_freq: figure saving frequency.
            figs_path: figures_path.
            n_figs: number of figures to save.
            load_weights: load pre-trained weights.
            save_weights: save model weights.
            weights_path: weights path.
        '''
        if load_weights:
            # load pre-trained weights
            self.load_weights(weights_path)
 
        clip_value = 0.01 # for clipping WGAN weights

        if save_figs:
            # save simulated images
            print('Saving simulated images...')
            simu_path = figs_path + '/simu'
            emul_path = figs_path + '/emul'
            shutil.rmtree(simu_path, ignore_errors=True)
            shutil.rmtree(emul_path, ignore_errors=True)
            Path(simu_path).mkdir(parents=True, exist_ok=True)
            Path(emul_path).mkdir(parents=True, exist_ok=True)

            # create a vector of parameters (it will be used to generate emulated images as well)
            sampled_params_plot = np.random.uniform(low=-1.0, high=1.0, size=(n_figs, self.params))

            # simulated images
            simu_imgs_plot = np.full(shape=(n_figs, self.img_rows, self.img_cols, self.channels),\
                                     fill_value=-1, dtype='float32')
            for index in range(n_figs):
                m, c, x0, x_steps = sampled_params_plot[index]
                self.fillPixelMap(m_m(m), m_c(c), m_x(x0), m_s(x_steps), simu_imgs_plot[index]) 

            # save images
            for i in range(n_figs):
                img = simu_imgs_plot[i].reshape(self.img_rows, self.img_cols).transpose()
                plt.imshow(img)
                plt.xlabel('x')
                plt.ylabel('y')
                plt.gca().invert_yaxis()
                plt.savefig(simu_path + '/' + str(i) + '.pdf', bbox_inches='tight', pad_inches = 0)
                plt.clf()

        print('###############')
        print('Pre-training:')
        for train_step in range(train_steps):
            log_mesg = '%d:' % train_step
            noise_value = 0.05

            ##########
            # Step 1 #
            ##########

            sampled_params = np.random.uniform(low=-1.0, high=1.0, size=(batch_size, self.params))

            # simulated images
            simu_imgs = np.full(shape=(batch_size, self.img_rows, self.img_cols, self.channels),\
                                fill_value=-1, dtype='float32')
            for index in range(batch_size):
                m, c, x0, x_steps = sampled_params[index]
                self.fillPixelMap(m_m(m), m_c(c), m_x(x0), m_s(x_steps), simu_imgs[index])

            # emulated images
            emul_imgs = self.emulator.predict(sampled_params)

            # noise: one-sided label smoothing (stochastic range) when training the siamese
            y_ones = np.random.uniform(low=0.7, high=1.0, size=(batch_size, 1))
            if self.WGAN:
                y_zeros = np.random.uniform(low=-1.0, high=-0.7, size=(batch_size, 1))
            else:
                y_zeros = np.zeros(shape=(batch_size, 1), dtype='float32')

            # train siamese
            s_loss_same = self.siamese.train_on_batch([simu_imgs, simu_imgs], y_ones)
            s_loss_diff = self.siamese.train_on_batch([simu_imgs, emul_imgs], y_zeros)
            s_loss = 0.5 * np.add(s_loss_same, s_loss_diff)
            log_mesg = '%s [S loss: %f]' % (log_mesg, s_loss[0])

            if self.WGAN:
                # Clip critic (siamese) weights
                for l in self.siamese.layers:
                    weights = l.get_weights()
                    weights = [np.clip(w, -clip_value, clip_value) for w in weights]
                    l.set_weights(weights)

            ##########
            # Step 2 #
            ##########

            sampled_params = np.random.uniform(low=-1.0, high=1.0, size=(batch_size, self.params))

            # simulated images
            simu_imgs = np.full(shape=(batch_size, self.img_rows, self.img_cols, self.channels),\
                                fill_value=-1, dtype='float32')
            for index in range(batch_size):
                m, c, x0, x_steps = sampled_params[index]
                self.fillPixelMap(m_m(m), m_c(c), m_x(x0), m_s(x_steps), simu_imgs[index])

            # noise: flip labels when training the emulator
            y_ones = np.ones(shape=(batch_size, 1), dtype='float32')
            for index in range(batch_size):
                if np.random.random() < noise_value:
                    y_ones[index] = 0.0

            # train emulator 
            a_loss = self.adversarial1.train_on_batch([simu_imgs, sampled_params], y_ones)
            log_mesg = '%s [E loss: %f]' % (log_mesg, a_loss[0])
            print(log_mesg)

            if save_figs and train_step>0 and train_step%save_freq==0:
                # save emulated_images
                print('Saving emulated images...')
                path = emul_path + '/' + str(train_step//save_freq)
                Path(path).mkdir(parents=True, exist_ok=True)
                emul_imgs_plot = self.emulator.predict(sampled_params_plot)
                for i in range(n_figs):
                    img = emul_imgs_plot[i].reshape(self.img_rows, self.img_cols).transpose()
                    plt.imshow(img)
                    plt.xlabel('x')
                    plt.ylabel('y')
                    plt.gca().invert_yaxis()
                    plt.savefig(path + '/' + str(i) + '.pdf', bbox_inches='tight', pad_inches = 0)
                    plt.clf()

        if save_weights:
            # save weights
            self.save_weights(weights_path)            

    def train(self, train_steps=1000000, batch_size=128, save_figs=False, save_freq=10000,\
              figs_path=None, n_figs=20, load_weights=False, save_weights=False, weights_path=None):
        ''' Training stage

        Args:
            train_steps: number of pre-training steps (iterations).
            batch_size: batch size.
            save_figs: save figures.
            save_freq: figure saving frequency.
            figs_path: figures_path.
            n_figs: number of figures to save.
            load_weights: load pre-trained weights.
            save_weights: save model weights.
            weights_path: weights path.
        '''
        if load_weights:
            # load pre-trained weights
            self.load_weights(weights_path)

        clip_value = 0.01 # for clipping WGAN weights

        if save_figs:
            # save true images
            print('Saving true images...')
            true_path = figs_path + '/true'
            emul_path = figs_path + '/gene_emul'
            shutil.rmtree(true_path, ignore_errors=True)
            shutil.rmtree(emul_path, ignore_errors=True)
            Path(true_path).mkdir(parents=True, exist_ok=True)
            Path(emul_path).mkdir(parents=True, exist_ok=True)

            # create a vector of parameters (it will be used to generate emulated images)
            sampled_noise_plot = np.random.normal(loc=0, scale=1.0, size=(n_figs, self.noise_size))

            # true images
            true_imgs_plot = np.full(shape=(batch_size, self.img_rows, self.img_cols, self.channels),\
                                     fill_value=-1, dtype='float32')
            for index in range(batch_size):
                m, c, x0, x_steps = self.data_params()
                self.fillPixelMap(m, c, x0, x_steps, true_imgs_plot[index]) 

            # save images 
            for i in range(n_figs):
                img = true_imgs_plot[i].reshape(self.img_rows, self.img_cols).transpose()
                plt.imshow(img)
                plt.xlabel('x')
                plt.ylabel('y')
                plt.gca().invert_yaxis()
                plt.savefig(true_path + '/' + str(i) + '.pdf', bbox_inches='tight', pad_inches = 0)
                plt.clf()

        print('###############')
        print('Training:')
        for train_step in range(train_steps):
            log_mesg = '%d:' % train_step
            noise_value = 0.05
 
            ##########
            # Step 1 #
            ##########

            sampled_noise = np.random.normal(loc=0, scale=1.0, size=(batch_size, self.noise_size))
            gene_params = self.generator.predict(sampled_noise)

            # true images
            true_imgs = np.full(shape=(batch_size, self.img_rows, self.img_cols, self.channels),\
                                fill_value=-1, dtype='float32')
            for index in range(batch_size):
                m, c, x0, x_steps = self.data_params()
                self.fillPixelMap(m, c, x0, x_steps, true_imgs[index])

            # simulated images
            simu_imgs = np.full(shape=(batch_size, self.img_rows, self.img_cols, self.channels),\
                                 fill_value=-1, dtype='float32')
            for index in range(batch_size):
                m, c, x0, x_steps = gene_params[index]
                self.fillPixelMap(m_m(m), m_c(c), m_x(x0), m_s(x_steps), simu_imgs[index])

            # noise: one-sided label smoothing (stochastic range) when training the discriminator
            y_ones = np.random.uniform(low=0.7, high=1.0, size=(batch_size, 1))
            if self.WGAN:
                y_zeros = np.random.uniform(low=-1.0, high=-0.7, size=(batch_size, 1))
            else:
                y_zeros = np.zeros(shape=(batch_size, 1), dtype='float32')

            # train discriminator
            d_loss_true = self.discriminator.train_on_batch(true_imgs, y_ones)
            d_loss_simu = self.discriminator.train_on_batch(simu_imgs, y_zeros)
            d_loss = 0.5 * np.add(d_loss_true, d_loss_simu)
            log_mesg = '%s [D loss: %f]' % (log_mesg, d_loss[0])

            if self.WGAN:
                # Clip critic weights
                for l in self.siamese.layers:
                    weights = l.get_weights()
                    weights = [np.clip(w, -clip_value, clip_value) for w in weights]
                    l.set_weights(weights)

            ##########
            # Step 2 #
            ##########

            sampled_noise = np.random.normal(loc=0, scale=1.0, size=(batch_size, self.noise_size))
            y_ones = np.ones(shape=(batch_size, 1), dtype='float32')

            # noise: flip labels when training the generator
            for index in range(batch_size):
                if np.random.random() < noise_value:
                    y_ones[index] = 0.0
 
            # train generator
            a_loss = self.adversarial2.train_on_batch(sampled_noise, y_ones)
            log_mesg = '%s [G loss: %f]' % (log_mesg, a_loss[0])
            print(log_mesg)

            if save_figs and train_step>0 and train_step%save_freq==0:
                # print generated parameters
                gene_params_plot = self.generator.predict(sampled_noise_plot).reshape(n_figs, self.params)
                for i in range(n_figs):
                    m, c, x0, x_steps = self.data_params()
                    print('Data:', m, c, x0, x_steps)
                    m, c, x0, x_steps = gene_params_plot[i]
                    print('Gene:', m_m(m), m_c(c), m_x(x0), m_s(x_steps))

                # save emulated_images
                print('Saving emulated images...')
                path = emul_path + '/' + str(train_step//save_freq)
                Path(path).mkdir(parents=True, exist_ok=True)
                emul_imgs_plot = self.emulator.predict(gene_params_plot)
                for i in range(n_figs):
                    img = emul_imgs_plot[i].reshape(self.img_rows, self.img_cols).transpose()
                    plt.imshow(img)
                    plt.xlabel('x')
                    plt.ylabel('y')
                    plt.gca().invert_yaxis()
                    plt.savefig(path + '/' + str(i) + '.pdf', bbox_inches='tight', pad_inches = 0)
                    plt.clf()

        if save_weights:
            # save weights
            self.save_weights(weights_path)

if __name__ == '__main__':
    magan = ModelAssistedGAN(noise_size=args.noise_size, params=args.params, img_rows=args.img_rows,\
                             img_cols=args.img_cols, channels=args.channels, WGAN=args.WGAN,\
                             print_summary=args.print_summary)
    if args.pretrain:
        magan.pretrain(train_steps=args.pretrain_steps, batch_size=args.batch_size,\
                       save_figs=args.save_figs, save_freq=args.save_freq, figs_path=args.figs_path,\
                       n_figs=args.n_figs, load_weights=args.load_weights, save_weights=args.save_weights,\
                       weights_path=args.weights_path)
    if args.train:
        magan.train(train_steps=args.train_steps, batch_size=args.batch_size,\
                    save_figs=args.save_figs, save_freq=args.save_freq, figs_path=args.figs_path,\
                    n_figs=args.n_figs, load_weights=args.load_weights, save_weights=args.save_weights,\
                    weights_path=args.weights_path)
