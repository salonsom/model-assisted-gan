# Model-Assisted Generative Adversarial Networks

Model-Assisted GAN code: [network architectures](networks.py) and [training script](train.py).

Based on: **Image-based model parameter optimization using Model-Assisted Generative Adversarial Networks** ([DOI](https://doi.org/10.1109/tnnls.2020.2969327), [arXiv](https://arxiv.org/abs/1812.00879)).

# Environment

```
python==3.6
tensorflow==2.1.0
tensorflow_addons==0.8.3
numpy==1.17.3
scipy==1.4.1
matplotlib==3.1.3
```

# Usage

```
python train.py
```

Optional arguments: 

| Parameter                 | Default       | Description   |	
| :------------------------ |:-------------:| :-------------|
| --img_rows | 32 | Image rows
| --img_cols | 32 | Image columns
| --channels | 1 | Image channels
| --params | 4 | Model parameters
| --noise_size | 100 | Noise size (generator input)
| --WGAN | False | Use Wasserstein GAN
| --pretrain | True | Run pre-training
| --train | False | Run training
| --pretrain_steps | 1000000 | Pre-training steps
| --train_steps | 1000000 | Training steps
| --batch_size | 128 | Batch size
| --print_summary | False | Print summary of the models
| --save_figs | False | Save figures
| --save_freq | 10000 | Figure saving frequency
| --figs_path | None | Figures path
| --n_figs | 20 | Number of images to save
| --load_weights | False | Load pre-trained weights
| --save_weights | False | Save weights
| --weights_path | None | Weights path
